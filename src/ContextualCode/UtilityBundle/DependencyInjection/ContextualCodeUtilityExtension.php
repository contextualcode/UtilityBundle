<?php

namespace ContextualCode\UtilityBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

class ContextualCodeUtilityExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        // load config
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        // load services
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}


<?php

namespace ContextualCode\UtilityBundle\Services;

use eZ\Publish\API\Repository\ContentService;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\LocationService;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\Core\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\API\Repository\Values\Content\Search\SearchHit;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Search\SearchResult;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\Core\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\ContentInfo;
use PDO;

class UtilityService
{
    /** @var Repository $repository */
    protected $repository;
    /** @var ContentService $contentService */
    protected $contentService;
    /** @var ContentTypeService $contentTypeService */
    protected $contentTypeService;
    /** @var LocationService $locationService */
    protected $locationService;
    /** @var SearchService $searchService */
    protected $searchService;

    public static $typeIdentifierMap;

    public function __construct(
        LocationService $locationService,
        ContentService $contentService,
        SearchService $searchService,
        ContentTypeService $contentTypeService,
        Repository $repository
    ) {
        $this->locationService = $locationService;
        $this->contentService = $contentService;
        $this->searchService = $searchService;
        $this->contentTypeService = $contentTypeService;
        $this->repository = $repository;
    }

    public function dateSort($items, $fieldIdentifier, $lang = 'eng-US', $dir = 'asc')
    {
         $asc = ($dir == 'asc');

         usort($items, function($a, $b) use($fieldIdentifier, $lang, $asc) {
            $aDate = $a['content']->getFieldValue($fieldIdentifier, $lang)->date;
            if (isset($aDate)) {
                $aDate = $aDate->format('U');
            }
            else {
                $aDate = $asc ? PHP_INT_MAX : PHP_INT_MIN;
            }
            $bDate = $b['content']->getFieldValue($fieldIdentifier, $lang)->date;
            if (isset($bDate)) {
                $bDate = $bDate->format('U');
            }
            else {
                $bDate = $asc ? PHP_INT_MAX : PHP_INT_MIN;
            }
            if ($aDate == $bDate) {
                return 0;
            }
            if ($asc) {
                return $aDate < $bDate ? -1 : 1;
            }
            return $aDate > $bDate ? -1 : 1;
        });

        return $items;
    }

    function getTypeIdentifierMap($controller) {
        if (!is_array(self::$typeIdentifierMap)) {

            $databaseHandler = $controller->get('ezpublish.connection');

            $conn = $databaseHandler->getConnection();

            $stmt = $conn->query("select id, identifier from ezcontentclass");

            $stmt->execute();

            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $map = array();

            foreach ($results as $type) {
                $map['type_'. $type['id']] = $type['identifier'];
            }

            self::$typeIdentifierMap = $map;
        }
        return self::$typeIdentifierMap;
    }

    public function sudoize($function_name, $args) {
        return $this->repository->sudo(
            function ($repository) use ($function_name, $args)
            {
                return call_user_func_array(array($this, $function_name), $args);
            }
        );
    }

    public function getParentLocationFromLocation($location)
    {
        $parentLocationID = $this->getParentLocationIDFromLocation($location);
        return $this->getLocationFromLocationID($parentLocationID);
    }

    public function getParentLocationIDFromLocation($location)
    {
        if (!$location instanceof Location) {
            return null;
        }
        $parentLocationID = $location->parentLocationId;
        return $parentLocationID;
    }

    public function getParentLocationIDFromLocationID($locationID)
    {
        $location = $this->locationService->loadLocation($locationID);
        if (!$location instanceof Location) {
            return null;
        }
        $parentLocationID = $location->parentLocationId;
        return $parentLocationID;
    }

    public function getLocationFromLocationID($locationID)
    {
        $location = $this->locationService->loadLocation($locationID);
        if (!$location instanceof Location) {
            return null;
        }
        return $location;
    }

    public function getContent($contentID, $lang = 'eng-US')
    {
        $langs = $this->getLangsFromLang($lang);
        try {
            return $this->contentService->loadContent($contentID, $langs);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getContentInfo($contentID, $lang = 'eng-US')
    {
        $langs = $this->getLangsFromLang($lang);
        try {
            return $this->contentService->loadContentInfo($contentID, $langs);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getMainLocationIDFromContentID($contentID)
    {
        if (!is_int($contentID)) {
            return null;
        }
        $content = $this->contentService->loadContent($contentID);
        if ($content instanceof Content) {
            $locationID= $content->contentInfo->mainLocationId;
            return $locationID;
        }
        return null;
    }

    public function getContentFromLocation($location, $lang = 'eng-US')
    {
        try {
            return $this->getContent($location->contentInfo->id, $lang);
        } catch( \Exception $e ) {
        }

        return null;
    }

    public function getContentFromLocationID($locationID, $lang = 'eng-US')
    {
        try {
            $location = $this->locationService->loadLocation($locationID);
            return $this->getContent($location->contentInfo->id, $lang);
        } catch( \Exception $e ) {
        }

        return null;
    }

    protected function getLangsFromLang($lang)
    {
        if ($lang === null) {
            $langs = null;
        }
        else if (is_array($lang)) {
            $langs = $lang;
        }
        else {
            $langs = array($lang);
        }
        return $langs;
    }

    public function getContentFromRelationAttribute($content, $attributeID, $lang = 'eng-US')
    {
        if (!$content instanceof Content) {
            return null;
        }

        $langs = $this->getLangsFromLang($lang);

        $fieldValue = $content->getFieldValue($attributeID);
        if (isset($fieldValue->destinationContentId)) {
            return $this->getContent($fieldValue->destinationContentId, $langs);
        } else if (isset($fieldValue->destinationContentIds)) {
            $contents = array();
            foreach ($fieldValue->destinationContentIds as $contentID) {
                $relatedContent = $this->getContent($contentID, $langs);
                if ($relatedContent instanceof Content) {
                    $contents[] = $relatedContent;
                }
            }
            return $contents;
        }

        return null;
    }

    public function getContentInfoFromRelationAttribute($content, $attributeID, $lang = 'eng-US')
    {
        if (!$content instanceof Content) {
            return null;
        }

        $langs = $this->getLangsFromLang($lang);

        $fieldValue = $content->getFieldValue($attributeID);
        if (isset($fieldValue->destinationContentId)) {
            return $this->getContentInfo($fieldValue->destinationContentId, $langs);
        } else if (isset($fieldValue->destinationContentIds)) {
            $contents = array();
            foreach ($fieldValue->destinationContentIds as $contentID) {
                $relatedContent = $this->getContentInfo($contentID, $langs);
                if ($relatedContent instanceof ContentInfo) {
                    $contents[] = $relatedContent;
                }
            }
            return $contents;
        }

        return null;
    }

    public function getAttributeID($content, $attributeID)
    {

        if (!$content instanceof Content || !isset($content->fields[$attributeID])) {
            return null;
        }

        $field = $content->getField($attributeID);

        return $field->id;
    }

    public function getFileAttributeFileName($content, $attributeID)
    {

        if (!$content instanceof Content || !isset($content->fields[$attributeID])) {
            return null;
        }

        $fieldValue = $content->getFieldValue($attributeID);

        return $fieldValue->fileName;
    }   

    public function getContentDownloadURL($content, $attributeID)
    {
        if (!$content instanceof Content || !isset($content->fields[$attributeID])) {
            return null;
        }

        $contentID = $content->id;
        $fileAttrID = $this->getAttributeID($content, $attributeID);
        $fileAttrName = $this->getFileAttributeFileName($content, $attributeID);

        return "/content/download/$contentID/$fileAttrID/file/$fileAttrName";
    }

    /**
     * Fetches a subtree with the given parameters.
     *
     * @param array $params
     * - An associative array of parameters:
     *
     * @param array|string|int|\eZ\Publish\Core\Repository\Values\Content\Location $params['parentLocation|parentLocations|parentLocationID|parentLocationIds']
     * - List of parent locations to limit fetch results too.
     *
     * @param array|string|int|\eZ\Publish\Core\Repository\Values\Content\Location $params['excludeLocation|excludeLocations|excludeLocationId|excludeLocationIds']
     * - List of parent locations to limit fetch results too.
     *
     * @param bool|null $params['loadContent']
     * - Load content for each item? (default is false)
     *
     * @param bool|null $params['ignoreVisibility'] 
     * - Ignore visibility for each item? (default is false)
     *
     * @param string|int|null $params['limit']
     * - Limit number of results? (default is null)
     *
     * @param int $params['offset']
     * - Offset number of results? (default is 0)
     *
     * @param string|int|null $params['depth']
     * - Limit depth of results to this value? (default is null)
     *
     * @param string|int|null $params['depthOperator']
     * - Operator to use for depth limit. Choose one of: 
     *   'contains', 'between', 'eq', 'gt', 'gte', 'in', 'lt', 'lte'.
     *   (default is 'eq')
     *
     * @param string|int|null $params['priority']
     * - Limit priority to this value? (default is null)
     *
     * @param string|null $params['languages']
     * - Limit content languages to this value (default is null)
     *
     * @param string|null $params['created']
     * - Limit creation date to this value (default is null)
     *
     * @param string|null $params['modified']
     * - Limit modified date to this value (default is null)
     * 
     * @param string|int|null $params['priorityOperator']
     * - Operator to use for priority limit. Choose one of: 
     *   'between', 'gt', 'gte', 'lt', 'lte'.
     *   (default is 'lte')
     *
     * @param array|string|int|null $params['sectionFilter']
     * - Limit to one section id or an array of section ids? (default is null)
     *
     * @param string|null $params['sectionFilterOperator']
     * - What operator to use to join the sectionFilter param values.
     *   Choose one of 'or', 'not'. (default is 'or')
     *
     * @param array|string|null $params['classFilter']
     * - Filter by a single class identifier, or an array of class identifiers. 
     *
     * @param string|null $params['classFilterOperator']
     * - What operator to use to join the classFilter param values.
     *   Choose one of 'or', 'not'. (default is 'or')
     * 
     * @param string|null $params['createdOperator']
     * - What operator to use to join the created param values.
     *   Choose one of 'or', 'not'. (default is 'or')
     * 
     * @param string|null $params['modifiedOperator']
     * - What operator to use to join the modified param values.
     *   Choose one of 'or', 'not'. (default is 'or')
     *
     * @param array|null $params['attributeFilter']
     * - Filter by an attribute. Should be an array with array values like:
     *    array(
     *        $field_identifier, $operator, $field_value
     *    )
     *    where $operator is one of: 
     *   'between', 'eq', 'gt', 'gte', 'in', 'like', 'lt', 'lte'.
     *
     * @param string|null $params['attributeFilterOperator']
     * - What operator to use to join the attributeFilter param values.
     *   Choose one of 'and', 'or', 'not'. (default is 'and')
     * 
     * @param array|null $params['relationFilter']
     * - Filter by an object relation field. Should be an array with array values like:
     *    array(
     *        $field_identifier, $operator, $field_value
     *    )
     *    where $operator is one of: 
     *   'in', 'contains'.
     * 
     * @param string|null $params['relationFilterOperator']
     * - What operator to use to join the relationFilter param values.
     *   Choose one of 'and', 'or', 'not'. (default is 'and')
     *
     * @param string|null $params['sortClause']
     * - Sort clause, one of:
     *    'path', 'published', 'modified', 'section', 
     *    'depth', 'priority', 'name', 'contentobject_id', null.
     *    (default is null, which is the parent location's sort clause).
     *
     * @param string|null $params['sortOrder']
     * - Sort order, one of:
     *   'asc', 'desc'.
     *   (default is null, which is the parent location's sort order).
     *
     * @param array|null $params['sortClauses']
     * - Array of sort clauses (check sortClause param for valid values).
     *    (default is array(null), which is the parent location's sort clause).
     *
     * @param string|null $params['sortOrders']
     * - Array of sort orders (check sortOrder param for valid values).
     *   Should match length of sortClauses param.
     *
     * ###################################
     *
     * A call from Twig to get all Banner objects in the current 
     * node might look like:
     *   {% set banners = cc_utilities.fetch({ 
     *         'parentLocation': location,
     *         'classFilter': 'banner',
     *         'depth': 1,
     *         'loadContent': true,
     *         'sortClause': 'priority',
     *         'sortOrder': 'desc'
     *      }) 
     *   %}
     *
     *
     */
    public function fetch($params)
    {
        // get parent location(s)
        $mainParentLocation = null;
        $parentLocationIds = array();
        $parentLocationParamNames = array("parentLocationID", "parentLocationIds", "parentLocation", "parentLocations");
        foreach ($parentLocationParamNames as $parentLocationParamName) {
            if (!isset($params[$parentLocationParamName]) || !$params[$parentLocationParamName]) {
                continue;
            }
            $value = $params[$parentLocationParamName];
            if (!is_array($value)) {
                $value = array($value);
            }
            foreach ($value as $item) {
                if ($item instanceof Location) {
                    $parentLocationIds[] = $item->id;
                    if (!$mainParentLocation) {
                        $mainParentLocation = $item;
                    }
                } elseif (is_scalar($item)) {
                    $parentLocationIds[] = intval($item);
                    if (!$mainParentLocation) {
                        $mainParentLocation = $this->getLocationFromLocationID($item);
                    }
                }
            }
        }

        // exclude locations
        $excludeLocationIds = array();
        $excludeLocationParamNames = array("excludeLocation", "excludeLocations", "excludeLocationId", "excludeLocationIds", "exclude");
        foreach ($excludeLocationParamNames as $excludeLocationParamName) {
            if (!isset($params[$excludeLocationParamName]) || !$params[$excludeLocationParamName]) {
                continue;
            }
            $value = $params[$excludeLocationParamName];
            if (!is_array($value)) {
                $value = array($value);
            }
            foreach ($value as $item) {
                if ($item instanceof Location) {
                    $excludeLocationIds[] = $item->id;
                } elseif (is_scalar($item)) {
                    $excludeLocationIds[] = intval($item);
                }
            }
        }

        // get params 
        $returnCount       = isset($params['returnCount'])      ? $params['returnCount'] : false;
        $returnQuery       = isset($params['returnQuery'])      ? $params['returnQuery'] : false;
        $loadContent       = isset($params['loadContent'])      ? $params['loadContent']      : false;
        $ignoreVisibility  = isset($params['ignoreVisibility']) ? $params['ignoreVisibility'] : false;
        $limit             = isset($params['limit'])            ? $params['limit']            : null;
        $offset            = isset($params['offset'])           ? $params['offset']           : 0;
        $depth             = isset($params['depth'])            ? $params['depth']            : null;
        $priority          = isset($params['priority'])         ? $params['priority']         : null;
        $languages         = isset($params['languages'])        ? $params['languages']        : null;
        $created           = isset($params['created'])          ? $params['created']          : null;
        $modified          = isset($params['modified'])         ? $params['modified']         : null;
        $sortClause        = isset($params['sortClause'])       ? $params['sortClause']       : null;
        $sortOrder         = isset($params['sortOrder'])        ? $params['sortOrder']        : null;
        $sortClauses       = isset($params['sortClauses'])      ? $params['sortClauses']      : null;
        $sortOrders        = isset($params['sortOrders'])       ? $params['sortOrders']       : null;
        $sectionFilter     = isset($params['sectionFiter'])     ? $params['sectionFilter']    : null;
        $classFilter       = isset($params['classFilter'])      ? $params['classFilter']      : null;
        $attributeFilter   = isset($params['attributeFilter'])  ? $params['attributeFilter']  : null;
        $relationFilter    = isset($params['relationFilter'])   ? $params['relationFilter']   : null;
        $depthOp           = isset($params['depthOperator'])    ? $params['depthOperator']    : 'eq';
        $priorityOp        = isset($params['priorityOperator']) ? $params['priorityOperator'] : 'lte';
        $sectionFilterOp   = isset($params['sectionFilterOperator'])   ? $params['sectionFilterOperator']   : 'or';
        $classFilterOp     = isset($params['classFilterOperator'])     ? $params['classFilterOperator']     : 'or';
        $attributeFilterOp = isset($params['attributeFilterOperator']) ? $params['attributeFilterOperator'] : 'and';
        $relationFilterOp  = isset($params['relationFilterOperator'])  ? $params['relationFilterOperator']  : 'and';
        $createdOp         = isset($params['createdOperator'])         ? $params['createdOperator']         : 'or';
        $modifiedOp        = isset($params['modifiedOperator'])        ? $params['modifiedOperator']        : 'or';

        // construct our query parts
        $criteria = array(
            'and' => array(),
            'or'  => array(),
            'not' => array()
        );
        
        // generate path string for main parent location
        if ($mainParentLocation) {
            // Fixing first version preview, in that case path string is like /1/2//x
            $parentPathString = str_replace('/x', '', $mainParentLocation->pathString);
        }

        // add parent location filter, use this one for single location filter to maintain compatibility?
        if ($mainParentLocation && count($parentLocationIds) <= 1) {
            $criteria['and'][] = new Criterion\Subtree($parentPathString);

        // add multi parent location filter
        } elseif ($parentLocationIds) {
            $subTreeStrings = array();
            foreach ($parentLocationIds as $parentLocationId) {
                try {
                    $parentLocation = $this->getLocationFromLocationID($parentLocationId);
                } catch (\eZ\Publish\Core\Base\Exceptions\NotFoundException $e) {
                    continue;
                }
                $subTreeStrings[] = str_replace(
                    '/x', '', $parentLocation->pathString
                );
            }
            $criteria['and'][] = new Criterion\Subtree($subTreeStrings);
        }

        // exclude selected location ids from results
        if ($excludeLocationIds) {
            $criteria["not"][] = new Criterion\LocationId($excludeLocationIds);
        }

        // add visibility check
        if (!$ignoreVisibility) {
            $criteria['and'][] = new Criterion\Visibility(Criterion\Visibility::VISIBLE);
        }

        // add depth limit
        if ($depth !== null && isset($parentPathString)) {
            // fix depth by offsetting by depth of our parent pathstring
            $depth += count(array_filter(explode('/', $parentPathString))) - 1 ;
            $criteria['and'][] = new Criterion\Location\Depth(
                                     self::translateOperator($depthOp), 
                                     $depth
                                 );
        }

        // add priority limit
        if ($priority !== null) {
            $criteria['and'][] = new Criterion\Location\Priority(
                                     self::translateOperator($priorityOp), 
                                     $priority
                                 );
        }

        // add languages filiter
        if ($languages) {
            $criteria['and'][] = new Criterion\LanguageCode($languages);
        }

        // add created filiter
        if ($created !== null) {
            if (is_array($created)) {
                foreach ($created as $key => $value) {
                    if ($value instanceof \DateTime) {
                        $created[$key] = $value->getTimestamp();
                    }
                }
            } elseif ($created instanceof \DateTime) {
                $created = $created->getTimestamp();
            }
            $criteria['and'][] = new Criterion\DateMetadata(
                Criterion\DateMetadata::CREATED,
                self::translateOperator($createdOp),
                $created
            );
        }

        // add modified filiter
        if ($modified !== null) {
            if (is_array($modified)) {
                foreach ($modified as $key => $value) {
                    if ($value instanceof \DateTime) {
                        $modified[$key] = $value->getTimestamp();
                    }
                }
            } elseif ($modified instanceof \DateTime) {
                $modified = $modified->getTimestamp();
            }
            $criteria['and'][] = new Criterion\DateMetadata(
                Criterion\DateMetadata::MODIFIED,
                self::translateOperator($modifiedOp),
                $modified
            );
        }

        // add section filter
        if ($sectionFilter) {
            if (!is_array($sectionFilter)) {
                $sectionFilter = array($sectionFilter);
            }
            if (in_array($sectionFilterOp,  array('or', 'not'))) {
                $criteria[$sectionFilterOp][] = new Criterion\SectionId($sectionFilter);
            }
        }

        // add class filter
        if ($classFilter) {
            if (!is_array($classFilter)) {
                $classFilter = array($classFilter);
            }
            if (in_array($classFilterOp, array('or', 'not'))) {
                $realClassFilterOp = $classFilterOp == 'or' ? 'and' : 'not';
                $criteria[$realClassFilterOp][] = new Criterion\ContentTypeIdentifier($classFilter);
            }
        }

        // add attribute filters
        if ($attributeFilter) {
            $attributeCriteria = array();
            foreach($attributeFilter as $f) {
                if (!is_array($f) || count($f) != 3) {
                    continue;
                }

                $attributeCriteria[] = new Criterion\Field(
                                           $f[0],
                                           self::translateOperator($f[1]), 
                                           $f[2]
                                       );
            }

            if (in_array($attributeFilterOp, array('and', 'or', 'not'))) {
                $criteria[$attributeFilterOp] = array_merge($criteria[$attributeFilterOp], $attributeCriteria);
            }
        }

        // add field relation filters
        if ($relationFilter) {
            $relationCriteria = array();
            foreach ($relationFilter as $f) {
                if (!is_array($f) || count($f) != 3) {
                    continue;
                }
                $relationCriteria[] = new Criterion\FieldRelation(
                    $f[0],
                    self::translateOperator($f[1]),
                    $f[2]
                );
            }
            if (in_array($relationFilterOp, array('and', 'or', 'not'))) {
                $criteria[$relationFilterOp] = array_merge($criteria[$relationFilterOp], $relationCriteria);
            }
        }

        // put all of our criterion together
        $finalCriteria = array(
            new Criterion\LogicalAnd($criteria['and'])
        );
        if (count($criteria['not'])) {
            foreach ($criteria['not'] as $c) {
                $finalCriteria[] = new Criterion\LogicalNot($c);
            }
        }
        if (count($criteria['or'])) {
            $finalCriteria[] = new Criterion\LogicalOr($criteria['or']);
        }

        // set sort clauses
        $finalSortClauses = array();
        if (isset($sortClauses)) {
            foreach ($sortClauses as $i => $clause) {
                if (isset($sortOrders[$i])) {
                    $order = $sortOrders[$i];
                }
                else {
                    // if length of sortOrder doesn't match up, 
                    // just use the default
                    $order = $sortOrder;
                }

                if ($mainParentLocation) $finalSortClauses[] = $this->translateSortClause($clause, $order, $mainParentLocation);
            }
        }
        else {
            if ($mainParentLocation) $finalSortClauses[] = $this->translateSortClause($sortClause, $sortOrder, $mainParentLocation);
        }

        $queryParams = array(
            'criterion' => new Criterion\LogicalAnd($finalCriteria),
            'sortClauses' => $finalSortClauses
        );
        if (isset($limit)) {
            $queryParams['limit'] = $limit;
        }
        if (isset($offset)) {
            $queryParams['offset'] = $offset;
        }

        // run the query
        $query = new LocationQuery($queryParams);

        if ($returnQuery) return $query;

        $results = $this->searchService->findLocations($query);

        $items = array();
        foreach ($results->searchHits as $searchHit) {
            
        if ($mainParentLocation && $mainParentLocation->contentInfo->id == $searchHit->valueObject->contentInfo->id) continue;

            $item = array(
                'location' => $searchHit->valueObject,
            );

            if ($loadContent) {
                $content = $this->contentService->loadContentByContentInfo($searchHit->valueObject->contentInfo);
                $item['content'] = $content;
            }

            $items[] = $item;
        }

        if ($returnCount) {
            return array(
                'totalCount' => $results->totalCount,
                'items' => $items
            );
        }

        return $items;
    }


    /**
     * Takes a sortClause string and a sortOrder string and returns a SortClause.
     * For valid values, check the docs on the function UtilityService::getSubtree().
     * If either are null, the value from the parent location is used.
     * @param string|null $sortClause
     * @param string|null $sortOrder
     * @param Location|null $parentLocation
     */      
    private function translateSortClause($sortClause, $sortOrder, $parentLocation = null)
    {
        if (is_array($sortClause)) {
            $sortOrder = $sortClause[2];
        }

        if (isset($sortOrder)) {
            switch ($sortOrder) {
                case 'asc':
                    $sortOrder = Query::SORT_ASC;
                    break;
                case 'desc':
                    $sortOrder = Query::SORT_DESC;
                    break;
                default:
                    $sortOrder = Query::SORT_DESC;
            }
        } else {
            $sortOrder = ($parentLocation->sortOrder == Location::SORT_ORDER_DESC) ? Query::SORT_DESC : Query::SORT_ASC;
        }

        if (is_array($sortClause)) {
            return new SortClause\Field($sortClause[0], $sortClause[1], $sortOrder);
        }

        if (isset($sortClause)) {
            switch ($sortClause) {
                case 'path':
                    return new SortClause\Location\Path($sortOrder);
                case 'published':
                    return new SortClause\DatePublished($sortOrder);
                case 'modified':
                    return new SortClause\DateModified($sortOrder);
                case 'section':
                    return new SortClause\SectionIdentifier($sortOrder);
                case 'depth':
                    return new SortClause\Location\Depth($sortOrder);
                case 'priority':
                    return new SortClause\Location\Priority($sortOrder);
                case 'name':
                    return new SortClause\ContentName($sortOrder);
                case 'contentobject_id':
                    return new SortClause\ContentId($sortOrder);
                default:
                    return new SortClause\Location\Path($sortOrder);
            }
        } else {
            return $this->getSortClauseFromLocationSort($parentLocation->sortField, $sortOrder);
        }
    }

    /**
     * Used by translateSortClause() to get a SortClause from a location's
     * sortField. Needs a valid Query::SORT_ASC or Query::SORT_DESC as $sortOrder.
     * @param Location::SORT_FIELD_* $sortField
     * @param Query::SORT_* $sortOrder
     */
    private function getSortClauseFromLocationSort($sortField, $sortOrder)
    {
        switch ($sortField) {
            case Location::SORT_FIELD_PATH:
                return new SortClause\Location\Path($sortOrder);

            case Location::SORT_FIELD_PUBLISHED:
                return new SortClause\DatePublished($sortOrder);

            case Location::SORT_FIELD_MODIFIED:
                return new SortClause\DateModified($sortOrder);

            case Location::SORT_FIELD_SECTION:
                return new SortClause\SectionIdentifier($sortOrder);

            case Location::SORT_FIELD_DEPTH:
                return new SortClause\Location\Depth($sortOrder);

            case Location::SORT_FIELD_PRIORITY:
                return new SortClause\Location\Priority($sortOrder);

            case Location::SORT_FIELD_NAME:
                return new SortClause\ContentName($sortOrder);

            case Location::SORT_FIELD_CONTENTOBJECT_ID: 
                return new SortClause\ContentId($sortOrder);

            default:
                return new SortClause\Location\Path($sortOrder);
        }
    }

    /**
     * Takes an operator string and returns a Criterion\Operator value.
     * For valid values, check the docs on the function UtilityService::getSubtree().
     * @param string|null $op The operator string.
     */      
    private static function translateOperator($op)
    {
        static $opMappings = array(
            'contains' => Criterion\Operator::CONTAINS,
            'between' => Criterion\Operator::BETWEEN,
            'eq'      => Criterion\Operator::EQ,
            'gt'      => Criterion\Operator::GT,
            'gte'     => Criterion\Operator::GTE,
            'in'      => Criterion\Operator::IN,
            'like'    => Criterion\Operator::LIKE,
            'lt'      => Criterion\Operator::LT,
            'lte'     => Criterion\Operator::LTE,
        );

        return isset($opMappings[$op]) ? $opMappings[$op] : Criterion\Operator::EQ;
    }
 
}


<?php

namespace ContextualCode\UtilityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeUtilityBundle extends Bundle
{
    protected $name = 'ContextualCodeUtilityBundle';
}


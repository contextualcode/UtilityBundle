<?php

namespace ContextualCode\UtilityBundle\Twig;

use eZ\Publish\Core\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\Core\Repository\Values\Content\Content;

class UtilityExtension extends \Twig_Extension
{

    public function __construct()
    {
    } 

    public function getFilters()
    {
        return array(
            new \Twig\TwigFilter('prioritySort', array($this, 'prioritySort'))
        );
    }

    public function getFunctions()
    {
        return array();
    }

    public function prioritySort($items)
    {
        usort($items, function($a, $b) {
            return $a['location']->priority > $b['location']->priority ? -1 : 1;
        });
        return $items;
    }
    
}


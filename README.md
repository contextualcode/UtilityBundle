# Utility Bundle
This is an eZ Publish 5 Symfony bundle to hold utilities.

## Installation
Run `composer require`:

```bash
$ composer require contextualcode/utility-bundle
```

Enable the bundle in `app/AppKernel.php` (`ezpublish/EzPublishKernel.php`) by adding this line in the `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\UtilityBundle\ContextualCodeUtilityBundle(),
            ...
        );
``` 

Add the UtilityService as a Twig global variable (in `app/config/config.yml` or `ezpublish/config/config.yml`):

```yml
twig:
    globals:
        cc_utilities: "@contextualcode.utility.utilityservice"
```

## Usage

There are many different utility functions provided by this bundle.

### `fetch`

Quick example of calling `fetch` from a Twig template to get all Banner objects in the current node:

```twig
{% set banners = cc_utilities.fetch({ 
      'parentLocation': location,
      'classFilter': 'banner',
      'depth': 1,
      'loadContent': true,
      'sortClause': 'priority',
      'sortOrder': 'desc'
   }) 
%}
```

This will return an array with each value having keys `location` and `content`.

Another example, to grab the closest `alert` item from the current node upwards with `persistent` attribute checked, sorted by priority and then name:

```twig
{% set paths = location.pathString|split('/')|reverse %}
{% set found_alert = false %}
{% for locationID in paths if not found_alert and locationID %}
    {% set alert = cc_utilities.fetch({
          'parentLocationID': locationID,
          'classFilter': 'alert',
          'sortClauses': [ 'priority', 'name' ],
          'sortOrders': [ 'desc', 'desc' ],
          'loadContent': true,
          'depth': 1,
          'limit': 1,
          'attributeFilter': [
              [ 'persistent', 'eq', true ]
          ]
       })
    %}
    {% set found_alert = alert|length > 0 %}
{% endfor %}
```

#### `fetch` Parameters

Required:

- `parentLocationID` or `parentLocation`
  - The parent location node ID to start the search from, or the actual location.

Optional:

- `loadContent`
    - Whether or not to load and return content as a `content` key of each item. 
    - Type: `bool`
    - Default: `false`
- `ignoreVisibility`
    - Whether or not to ignore visibility when searching.
    - Type: `bool`
    - Default: `false`
- `limit`
    - The limit of the number of results.
    - Type: `string` or `int` or `null`
    - Default: `null`
- `offset`
    - The offset of the results.
    - Type: `int`
    - Default: `0`
- `depth`
    - Limit results to this depth from the `parentLocation`.
    - Pair with `depthOperator`.
    - Type: `string` or `array` or `int` or `null`
    - Default: `null`
- `depthOperator`
    - Operator to use for the `depth` limit.
    - Type: `string`, one of: `'contains', 'between'`, `'eq'`, `'gt'`, `'gte'`, `'in'`, `'lt'`, `'lte'`
    - Default: `'eq'`
- `priority`
    - Limit results to this priority.
    - Pair with `priorityOperator`.
    - Type: `string` or `array` or `int` or `null`
    - Default: `null`
- `priorityOperator`
    - Operator to use for the `priority` limit.
    - Type: `string`, one of: `'between'`, `'gt'`, `'gte'`, `'lt'`, `'lte'`
    - Default: `'lte'`
- `sectionFilter`
    - Limit results to one section ID or an array of section IDs.
    - Pair with `sectionFilterOperator`.
    - Type: `string` or `array` or `int` or `null`
    - Default: `null`
- `sectionFilterOperator`
    - Operator to use for the `sectionFilter`.
    - Type: `string`, one of: `'or'`, `'not'`
    - Default: `'or'`
- `classFilter`
    - Limit results to one class ID or an array of class IDs.
    - Pair with `classFilterOperator`.
    - Type: `string` or `array` or `int` or `null`
    - Default: `null`
- `classFilterOperator`
    - Operator to use for the `classFilter`.
    - Type: `string`, one of: `'or'`, `'not'`
    - Default: `'or'`
- `attributeFilter`
    - Filter by an attribute. Should be an array with array values like: `[ $field_identifier, $operator, $field_value ]`, where `$operator` is one of: `'between'`, `'eq'`, `'gt'`, `'gte'`, `'in'`, `'like'`, `'lt'`, `'lte'`
    - Type: `array` of `array`s or `null`
    - Default: `null`
- `attributeFilterOperator`
    - Operator to use for the `attributeFilter`.
    - Type: `string`, one of: `'and'`, `'or'`, `'not'`
    - Default: `'and'`
- `sortClause`
    - Single sort clause to use. (If you want to specifiy multiple sort clauses, use `sortClauses`).
    - Pair with `sortOrder`.
    - Type: `null` (which uses the parent location's sort clause), or `string`, one of: `'path'`, `'published'`, `'modified'`, `'section'`, `'depth'`, `'priority'`, `'name'`, `'contentobject_id'`
    - Default: `null` (which uses the parent location's sort clause)
- `sortOrder`
    - Single sort order to use.
    - Pair with `sortClause`.
    - Type: `null` (which uses the parent location's sort order), or `string`, one of: `'asc'`, `'desc'`
    - Default: `null` (which uses the parent location's sort order)
- `sortClauses`
    - Same as `sortClause`, but you can pass multiple as an array (for secondary, tertiary, etc. sorting).
    - Pair with `sortOrders`.
    - Type: `array` of valid values for `sortClause`.
    - Default: `array(null)`
- `sortOrders`
    - Same as `sortOrder`, but you can pass multiple as an array (for secondary, tertiary, etc. sorting).
    - Each one will pair with the value in the same index in `sortClauses`. If the `sortClauses` and `sortOrders` are different lengths, the defaults are used for the missing values. 
    - Type: `array` of valid values for `sortOrders`.
    - Default: `array(null)`

### `getContent($contentID, $lang = 'eng-US')`

### `getContentFromLocationID($locationID, $lang = 'eng-US')`

### `getContentFromLocation($location, $lang = 'eng-US')`

### `getContentInfo($contentID, $lang = 'eng-US')`

### `getParentLocationFromLocation($location)`

### `getParentLocationIDFromLocation($location)`

### `getParentLocationIDFromLocationID($locationID)`

### `getParentLocationIDFromLocationID($locationID)`

### `getParentLocationIDFromLocationID($locationID)`

### `getMainLocationIDFromContentID($contentID)`

### `getContentFromRelationAttribute($content, $attributeID, $lang = 'eng-US')`

If passed an `$attributeID` for an Object Relation attribute, content will be returned.
If passed an `$attributeID` for an Object Relations attribute, an array of content will be returned.

### `getContentInfoFromRelationAttribute($content, $attributeID, $lang = 'eng-US')`

If passed an `$attributeID` for an Object Relation attribute, content info will be returned.
If passed an `$attributeID` for an Object Relations attribute, an array of content infos will be returned.

### `getAttributeID($content, $attributeID)`

### `getFileAttributeFileName($content, $attributeID)`

### `getContentDownloadURL($content, $attributeID)`

More documentation to come.

